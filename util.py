import re


def is_valid_version_name(release_name):

    valid_repos = ['commons', 'auth', 'front', 'file', 'ocr', 'operator-queue', 'id-ui', 'app-ui', 'operator-ui']
    name, _ = release_name.split('_')
    pattern = re.compile('^' + name + '_[0-9]+\\.[0-9]+\\.[0-9]+$')
    if (valid_repos.count(name) > 0) and pattern.match(release_name):
        return True
    else:
        return False


def remove_invalid_versions(versions):

    valid_versions = []
    for version in versions:
        if is_valid_version_name(version.name):
            valid_versions.append(version)

    return valid_versions


def get_versions_by_date(versions, date):

    filtered_versions = []
    for version in versions:
        if version.release_date == date:
            filtered_versions.append(version)

    return filtered_versions
