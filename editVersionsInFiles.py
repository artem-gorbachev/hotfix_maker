def forward_to_change_file(file_destination, pr, task_key, current_commons, next_commons_version):
    if file_destination == pr.repo + '/src/environments/version.ts':
        change_ui(file_destination, pr, task_key)
    elif file_destination == pr.repo + '/build.sbt':
        change_play_build(file_destination, pr, task_key, current_commons, next_commons_version)
    elif file_destination == pr.repo + '/conf/version':
        change_play_versions(file_destination, pr, task_key)
    elif file_destination == pr.repo + '/pom.xml':
        change_commons(file_destination, pr, task_key)


def change_ui(file_destination, pr, task_key):
    line = pr.current_version + "." + task_key
    with open(file_destination, "r") as file:
        file_data = file.read()

    file_data = file_data.replace(line, pr.next_version)

    with open(file_destination, "w") as file:
        file.write(file_data)


def change_commons(file_destination, pr, task_key):
    line = pr.current_version + "." + task_key + "-SNAPSHOT"
    with open(file_destination, "r") as file:
        file_data = file.read()

    file_data = file_data.replace(line, pr.next_version)

    with open(file_destination, "w") as file:
        file.write(file_data)


def change_play_versions(file_destination, pr, task_key):
    line = pr.current_version + "." + task_key
    with open(file_destination, "r") as file:
        file_data = file.read()

    file_data = file_data.replace(line, pr.next_version)

    with open(file_destination, "w") as file:
        file.write(file_data)


def change_play_build(file_destination, pr, task_key, current_commons, next_commons_version):
    line = 'version in ThisBuild := "' + pr.current_version + "." + task_key + '"'
    newline = 'version in ThisBuild := "' + pr.next_version + '"'
    with open(file_destination, "r") as file:
        file_data = file.read()

    file_data = file_data.replace(line, newline)

    if current_commons:

        commons_line = 'val enteraCommonsVersion = "' + current_commons + "." + task_key + "-SNAPSHOT" + '"'
        new_commons_line = 'val enteraCommonsVersion = "' + next_commons_version + '"'

        file_data = file_data.replace(commons_line, new_commons_line)

    with open(file_destination, "w") as file:
        file.write(file_data)
