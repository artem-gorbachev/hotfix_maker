import requests
from requests.auth import HTTPBasicAuth
import json
from config import *
from jiraVersion import JiraVersion
from util import *


class JiraClient:
    """В этом классе собраны запросы к Jira Cloud API"""

    def get_issue_id(self, key):
        """Возвращает внутренний ID для задачи по ключу задачи"""

        url = 'https://' \
              + atlassian_domain.get('jira_domain') \
              + '.atlassian.net/rest/api/3/issue/' + key
        auth = HTTPBasicAuth(jira_auth.get('login'), jira_auth.get('token'))
        headers = {"Accept": "application/json"}
        response = requests.request(
            "GET",
            url,
            headers=headers,
            auth=auth
        )

        assert (response.status_code == 200), "Could not get issue by key"

        id = json.loads(response.text)['id']

        return id

    def get_issue_repos(self, id):
        """Возвращает список репозиториев, которые были затронуты в задаче"""
        url = 'https://' \
              + atlassian_domain.get('jira_domain') \
              + '.atlassian.net/rest/dev-status/1.0/issue/detail?issueId=' \
              + id \
              + '&applicationType=bitbucket&dataType=pullrequest'
        auth = HTTPBasicAuth(jira_auth.get('login'), jira_auth.get('token'))
        headers = {"Accept": "application/json"}
        response = requests.request(
            "GET",
            url,
            headers=headers,
            auth=auth
        )

        assert (response.status_code == 200), "Could not get issue repos info"

        repos_info = json.loads(response.text)

        branches = repos_info['detail'][0]['branches']
        repos = []
        for branches in branches:
            repos.append(branches['repository'])
        affected_repos = []
        for repo in repos:
            affected_repos.append(repo['name'])

        return affected_repos

    def get_repo_versions(self, repo):

        """Возвращает последнюю страницу релизов по вхождению названия репозитория, отсортированных по дате релиза по
        убыванию. Возвращает только валидные релизы.
        Было принято решение отказаться от пагинации, т.к. если мы запрашиваем релизы, которые имеют статус Released и
        при этом в имени имеют название репозитория, то нам пришлось бы листать страницы только в том случае, если таких
        релизов ЗА ДЕНЬ будет больше, чем max_results, а это почти невероятно."""

        max_results = 50

        url = 'https://' \
              + atlassian_domain.get('jira_domain') \
              + '.atlassian.net/rest/api/3/project/' \
              + atlassian_domain.get('jira_project') \
              + '/version?orderBy=-releaseDate&status=released' \
              + '&maxResults=' + str(max_results) + '&query=' + repo
        auth = HTTPBasicAuth(jira_auth.get('login'), jira_auth.get('token'))
        headers = {"Accept": "application/json"}
        response = requests.request(
            "GET",
            url,
            headers=headers,
            auth=auth
        )

        assert (response.status_code == 200), "Could not get Jira releases by repo"

        versions = []
        for element in json.loads(response.text)['values']:
            if is_valid_version_name(element['name']):
                repo, version_part = element['name'].split('_')
                major_version, minor_version, bugfix_version = version_part.split('.')
                version = JiraVersion(
                    # name=element['name'],
                    release_date=element['releaseDate'],
                    repo=repo,
                    major_version=int(major_version),
                    minor_version=int(minor_version),
                    bugfix_version=int(bugfix_version)
                )

                versions.append(version)

        return versions

    def create_version(self, version):
        url = 'https://' \
              + atlassian_domain.get('jira_domain') \
              + '.atlassian.net/rest/api/3/version'
        auth = HTTPBasicAuth(jira_auth.get('login'), jira_auth.get('token'))
        headers = {"Accept": "application/json", "Content-Type": "application/json"}
        payload = json.dumps({
            "name": version.name,
            "projectId": 10000
        })
        response = requests.request(
            "POST",
            url,
            data=payload,
            headers=headers,
            auth=auth
        )

        assert (response.status_code == 201), "Could not create Jira version " + response.text

    def link_versions(self, issue_key, versions):
        # Сначала подготовим названия версий таким образом, чтобы они представляли json-объекты.
        version_names = []
        for version in versions:
            name = json.loads('{"name":"' + version.name + '"}')
            version_names.append(name)

        url = 'https://' \
              + atlassian_domain.get('jira_domain') \
              + '.atlassian.net/rest/api/3/issue/' + issue_key
        auth = HTTPBasicAuth(jira_auth.get('login'), jira_auth.get('token'))
        headers = {"Accept": "application/json", "Content-Type": "application/json"}
        payload = json.dumps({
            "update": {
                "fixVersions": [
                    {"set": version_names}
                ]
            }
        })
        response = requests.request(
            "PUT",
            url,
            data=payload,
            headers=headers,
            auth=auth
        )

        assert (response.status_code == 204), "Could not link Jira versions to an issue " + response.text
