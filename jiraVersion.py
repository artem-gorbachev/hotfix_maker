class JiraVersion:

    def __init__(self, release_date, repo, major_version, minor_version, bugfix_version: int):
        self.release_date = release_date
        self.repo = repo
        self.major_version = major_version
        self.minor_version = minor_version
        self.bugfix_version = bugfix_version

    def __lt__(self, other):
        if self.major_version < other.major_version:
            return True
        elif self.major_version > other.major_version:
            return False
        else:
            if self.minor_version < other.minor_version:
                return True
            elif self.minor_version > other.minor_version:
                return False
            else:
                if self.bugfix_version < other.bugfix_version:
                    return True
                else:
                    return False

    def __eq__(self, other):
        if self.major_version != other.major_version:
            return False
        else:
            if self.minor_version != other.minor_version:
                return False
            else:
                if self.bugfix_version != other.bugfix_version:
                    return False
                else:
                    return True

    def __str__(self):
        return self.repo + ' ' \
               + str(self.major_version) + '.' \
               + str(self.minor_version) + '.' \
               + str(self.bugfix_version)

    @property
    def name(self):
        return self.repo + '_' \
               + str(self.major_version) + '.' \
               + str(self.minor_version) + '.' \
               + str(self.bugfix_version)

    @property
    def version(self):
        return str(self.major_version) + '.' \
               + str(self.minor_version) + '.' \
               + str(self.bugfix_version)

    def get_next(self):
        self.bugfix_version += 1
        return self
