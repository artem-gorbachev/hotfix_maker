class ConflictChecker:

    def is_conflict_free(self, pr):
        for ds in pr.diff_status:
            if ds['status'] == 'merge conflict':
                break
                return False
            else:
                return True

    def has_acceptable_conflicts(self, pr):
        pr_conflicts = []
        for ds in pr.diff_status:
            if ds['status'] == 'merge conflict':
                pr_conflicts.append(ds['old']['path'])
        if type(pr.filename) == list:
            if len(pr_conflicts) == 2 and pr.filename[0] in pr_conflicts and pr.filename[1] in pr.filename:
                return True
            else:
                return False
        else:
            if len(pr_conflicts) == 1 and pr.filename == pr_conflicts[0]:
                return True
            else:
                return False

