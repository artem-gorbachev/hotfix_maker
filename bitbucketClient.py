import requests
from requests.auth import HTTPBasicAuth
import json
from config import *


class BitbucketClient:

    def get_last_master_commit(self, repo):
        """Возвращает хеш последнего коммита на ветке master. Он понадобится нам, чтобы от этого коммита создавать
        новую ветку."""
        url = 'https://bitbucket.org/api/2.0/repositories/' \
              + atlassian_domain.get('bitbucket_profile') \
              + '/' + repo + '/commits/master'
        auth = HTTPBasicAuth(bitbucket_auth.get('login'), bitbucket_auth.get('password'))
        headers = {"Accept": "application/json"}
        response = requests.request(
            "GET",
            url,
            headers=headers,
            auth=auth
        )

        return json.loads(response.text)['values'][0]['hash']

    def create_hotfix_branch(self, version):
        branch_name = 'hotfix/' + version.version
        master_hash = self.get_last_master_commit(version.repo)
        url = 'https://bitbucket.org/api/2.0/repositories/' \
              + atlassian_domain.get('bitbucket_profile') \
              + '/' + version.repo + '/refs/branches'
        auth = HTTPBasicAuth(bitbucket_auth.get('login'), bitbucket_auth.get('password'))
        headers = {"Accept": "application/json", "Content-Type": "application/json"}
        payload = json.dumps({
            "name": branch_name,
            "target": {
                "hash": master_hash
            }
        })
        response = requests.request(
            "POST",
            url,
            data=payload,
            headers=headers,
            auth=auth
        )

        assert (response.status_code == 201), "Could not create hotfix branch for " + version.repo + ":" + response.text

    def get_open_pull_requests(self, repo):
        """Возвращает массив всех открытых пулл-реквестов для заданного репозитория"""
        url = 'https://bitbucket.org/api/2.0/repositories/' \
              + atlassian_domain.get('bitbucket_profile') \
              + '/' + repo + '/pullrequests?state=OPEN&pagelen=50'
        auth = HTTPBasicAuth(bitbucket_auth.get('login'), bitbucket_auth.get('password'))
        headers = {"Accept": "application/json"}
        response = requests.request(
            "GET",
            url,
            headers=headers,
            auth=auth
        )

        assert (response.status_code == 200), "Could not get pull request list for " + repo + ":" + response.text

        return json.loads(response.text)['values']

    def get_current_pull_requests(self, pr):
        """Возвращает массив всех открытых пулл-реквестов для заданного репозитория"""
        url = 'https://bitbucket.org/api/2.0/repositories/' \
              + atlassian_domain.get('bitbucket_profile') \
              + '/' + pr.repo + '/pullrequests/' + str(pr.id)
        auth = HTTPBasicAuth(bitbucket_auth.get('login'), bitbucket_auth.get('password'))
        headers = {"Accept": "application/json"}
        response = requests.request(
            "GET",
            url,
            headers=headers,
            auth=auth
        )

        assert (response.status_code == 200), "Could not get pull request list for " + pr.repo + ":" + response.text

        return json.loads(response.text)

    def get_pr_conflict_status(self, pr):
        """Возвращает массив всех открытых пулл-реквестов для заданного репозитория"""
        url = 'https://bitbucket.org/api/2.0/repositories/' \
              + atlassian_domain.get('bitbucket_profile') \
              + '/' + pr.repo + '/pullrequests/' + str(pr.id) + '/diffstat'
        auth = HTTPBasicAuth(bitbucket_auth.get('login'), bitbucket_auth.get('password'))
        headers = {"Accept": "application/json"}
        response = requests.request(
            "GET",
            url,
            headers=headers,
            auth=auth
        )

        assert (response.status_code == 200), "Could not get pull request list for " + pr.repo + ":" + response.text

        return json.loads(response.text)['values']

    def get_changes(self, pr):
        """Возвращает массив всех открытых пулл-реквестов для заданного репозитория"""
        url = 'https://bitbucket.org/api/2.0/repositories/' \
              + atlassian_domain.get('bitbucket_profile') \
              + '/' + pr.repo + '/pullrequests/' + str(pr.id) + '/diff'
        auth = HTTPBasicAuth(bitbucket_auth.get('login'), bitbucket_auth.get('password'))
        headers = {"Accept": "application/json"}
        response = requests.request(
            "GET",
            url,
            headers=headers,
            auth=auth
        )

        assert (response.status_code == 200), "Could not get pull request list for " + pr.repo + ":" + response.text

        return response.text

    def update_pull_request(self, pr):
        """Обновляет пулл-реквест в Bitbucket после того, как мы изменили его свойства локально (в коде)."""
        url = 'https://bitbucket.org/api/2.0/repositories/' \
              + atlassian_domain.get('bitbucket_profile') \
              + '/' + pr.repo + '/pullrequests/' + str(pr.id)
        auth = HTTPBasicAuth(bitbucket_auth.get('login'), bitbucket_auth.get('password'))
        headers = {"Accept": "application/json", "Content-Type": "application/json"}
        payload = json.dumps({
            "title": pr.title,
            "destination": {
                "branch": {
                    "name": pr.destination_branch
                }
            }
        })
        response = requests.request(
            "PUT",
            url,
            data=payload,
            headers=headers,
            auth=auth
        )

        assert (response.status_code == 200), "Could not update pull request list for PR " + str(
            pr) + ":" + response.text

    def approve_pr(self, pr):
        """Метод для присвоения approved для пулл-реквеста"""
        url = 'https://bitbucket.org/!api/2.0/repositories/' \
              + atlassian_domain.get('bitbucket_profile') \
              + '/' + pr.repo + '/pullrequests/' + str(pr.id) + '/approve'
        auth = HTTPBasicAuth(bitbucket_auth.get('login'), bitbucket_auth.get('password'))
        headers = {"Accept": "application/json"}
        response = requests.request(
            "POST",
            url,
            headers=headers,
            auth=auth
        )
        if response.status_code == 200:
            print("Пулл-реквест для " + pr.repo + " " + pr.source_branch + " -> " + pr.destination_branch + " approved!")

    def merge_pr(self, pr):
        """Метод merge'а для пулл-реквеста. Возвращает id коммита merge'a"""
        url = 'https://bitbucket.org/!api/2.0/repositories/' \
              + atlassian_domain.get('bitbucket_profile') \
              + '/' + pr.repo + '/pullrequests/' + str(pr.id) + '/merge'
        auth = HTTPBasicAuth(bitbucket_auth.get('login'), bitbucket_auth.get('password'))
        headers = {"Accept": "application/json"}
        response = requests.request(
            "POST",
            url,
            headers=headers,
            auth=auth
        )
        if response.status_code == 200:
            print("Пулл-реквест для " + pr.repo + " " + pr.source_branch + " -> " + pr.destination_branch + " merged!")
            return json.loads(response.text)['merge_commit']['hash']

    def create_pr(self, pr):
        """Создает пулл-реквест. Возвращает id созданного пулл-реквеста"""
        url = 'https://bitbucket.org/api/2.0/repositories/' \
              + atlassian_domain.get('bitbucket_profile') \
              + '/' + pr.repo + '/pullrequests/'
        auth = HTTPBasicAuth(bitbucket_auth.get('login'), bitbucket_auth.get('password'))
        headers = {"Accept": "application/json", "Content-Type": "application/json"}
        payload = json.dumps(
            {
                "title": "Hotfix/" + pr.next_version,
                "description": "",
                "source": {
                    "branch": {
                        "name": pr.source_branch
                    }
                },
                "destination": {
                    "branch": {
                        "name": pr.destination_branch
                    }
                },
                "reviewers": []}
        )
        response = requests.request(
            "POST",
            url,
            data=payload,
            headers=headers,
            auth=auth
        )

        if response.status_code == 201:
            return str(json.loads(response.text)['id'])

    def get_branch(self, pr):
        """Возвращает хеш ветки. Он понадобится нам для скачивания файлов из ветки"""
        url = 'https://bitbucket.org/api/2.0/repositories/' \
              + atlassian_domain.get('bitbucket_profile') + "/" \
              + pr.repo + '/refs/branches/' + pr.source_branch
        auth = HTTPBasicAuth(bitbucket_auth.get('login'), bitbucket_auth.get('password'))
        headers = {"Accept": "application/json"}
        response = requests.request(
            "GET",
            url,
            headers=headers,
            auth=auth
        )
        return json.loads(response.text)

    def get_file_from_branch(self, pr, filename):
        """Метод загружает указанные файлы на локальную машину. Возвращает контент файла."""
        url = 'https://api.bitbucket.org/2.0/repositories/' \
              + atlassian_domain.get('bitbucket_profile') + "/" \
              + pr.repo + '/src/' \
              + pr.branch_hash + '/' + filename + '?at=' + pr.source_branch
        auth = HTTPBasicAuth(bitbucket_auth.get('login'), bitbucket_auth.get('password'))
        headers = {"Accept": "application/json"}
        response = requests.request(
            "GET",
            url,
            headers=headers,
            auth=auth
        )

        print(url)
        return response

    def upload_file(self, pr, filename, file_destination):
        """Метод загружает файл в нужную ветку и делает коммит"""
        url = 'https://bitbucket.org/api/2.0/repositories/' \
              + atlassian_domain.get('bitbucket_profile') + "/" \
              + pr.repo + '/src/'
        auth = HTTPBasicAuth(bitbucket_auth.get('login'), bitbucket_auth.get('password'))
        headers = {"Accept": '*/*', 'Content-Type': "application/x-www-form-urlencoded", }
        with open(file_destination, "rb") as content_file:
            content = content_file.read()
        data = {
            "message": (None, "Hotfix " + pr.next_version),
            "branch": (None, pr.source_branch),
            filename: (None, content)
        }

        response = requests.request(
            "POST",
            url,
            data=data,
            headers=headers,
            auth=auth
        )

        return response

    def add_tag_to_merge_commit(self, pr):
        """Метод добавляет тег на коммит merge'а"""
        url = 'https://bitbucket.org/api/2.0/repositories/' \
              + atlassian_domain.get('bitbucket_profile') + "/" \
              + pr.repo + '/refs/tags/'
        auth = HTTPBasicAuth(bitbucket_auth.get('login'), bitbucket_auth.get('password'))
        headers = {"Accept": "application/json", "Content-Type": "application/json"}
        payload = json.dumps(
            {
                "name": pr.next_version,
                "target": {
                    "hash": pr.merge_hash,
                }
            }
        )
        response = requests.request(
            "POST",
            url,
            data=payload,
            headers=headers,
            auth=auth
        )
        if response.status_code == 201:
            print('Тэг ' + pr.next_version + ' создан и задан для коммита ' + pr.merge_hash)
        else:
            print('Не удалось добавить тег')

