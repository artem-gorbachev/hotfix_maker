class PullRequest:

    def __init__(self, repo, source_branch, destination_branch, id, title):
        self.repo = repo
        self.source_branch = source_branch
        self.destination_branch = destination_branch
        self.id = id
        self.title = title

    def __str__(self):
        return self.repo + ': (' + self.title + ')' \
               + ' from ' + self.source_branch \
               + ' to ' + self.destination_branch \
               + ' id = ' + str(self.id)

    def get_file_names(self):
        if self.repo in ['id-ui', 'app-ui', 'operator-ui']:
            filename = 'src/environments/version.ts'
        elif self.repo in ['auth', 'front', 'file', 'ocr', 'operator-queue']:
            filename = ['build.sbt', 'conf/version']
        elif self.repo == 'commons':
            filename = 'pom.xml'
        return filename

