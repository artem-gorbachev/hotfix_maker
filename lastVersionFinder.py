from jiraClient import JiraClient


class LastVersionFinder:

    def get_latest_version_for(self, repo):
        jira_client = JiraClient()
        versions = jira_client.get_repo_versions(repo)
        versions = sorted(versions, reverse=True)

        return versions[0]
