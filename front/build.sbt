//region ThisBuild

name := "front"
organization in ThisBuild := "pro.entera"
scalaVersion in ThisBuild := "2.12.6"
version in ThisBuild := "1.0.14"

// Включает отображение стек-трейса, когда тест падает, чтобы понимать, где происходит падение.
testOptions in ThisBuild += Tests.Argument(TestFrameworks.JUnit, "-a")
javaOptions += "-Dfile.encoding=UTF-8"

//endregion
//region Project

lazy val frontend = (project in file("."))
  .enablePlugins(PlayJava, PlayEbean)
  .settings(
    libraryDependencies ++= Seq(
      dependencies.enteraCommons,
      // Добавляем Play WS
      ws,
    ),
    PlayKeys.playDefaultPort := 9001,
    // Данная настройка введена для работы интеграции с Моим Делом, т.к. запрос к Моему Делу может долго выполняться.
    PlayKeys.devSettings += "play.server.http.idleTimeout" -> "10 minutes"
  )

//endregion
//region Dependencies

lazy val dependencies =
  new {
    //region Versions

    val enteraCommonsVersion = "1.0.11"
    val playMailerVersion = "6.0.1"

    //endregion
    //region Artifacts

    // Entera shared libraries.
    val enteraCommons = "pro.entera" % "commons" % enteraCommonsVersion

    //endregion
  }

//endregion
