from jiraTask import JiraTask
from jiraClient import JiraClient
from lastVersionFinder import LastVersionFinder
from conflictChecker import ConflictChecker
from bitbucketClient import BitbucketClient
from pullRequest import PullRequest
from newPullRequest import NewPullRequest
from editVersionsInFiles import forward_to_change_file

# issue_key = 'AG-20'
#
# issue = JiraTask(issue_key)
#
# jira_client = JiraClient()
#
# # Получаем Issue Id в обмен на Issue Key, т.к. для дальнейших операций нам понабится именно он
# issue.id = jira_client.get_issue_id(issue.key)
# print("id задачи в Jira: " + issue.id)
#
# # Шаг 1. Получаем список репозиториев, которые были затронуты в задаче
# affected_repos = jira_client.get_issue_repos(issue.id)
# print(affected_repos)
#
# # Ставим commons на первое место, если он есть, ибо начинать будем с него.
# if "commons" in affected_repos:
#     index = affected_repos.index("commons")
#     if index != 0:
#         temp_element = affected_repos[0]
#         affected_repos[0] = affected_repos[index]
#         affected_repos[index] = temp_element
# print(affected_repos)
#
# last_version_finder = LastVersionFinder()
#
# # Шаг 2. Получаем список версий (релизов), которые будут созданы под создаваемый хотфикс
# next_versions = []
# for repo in affected_repos:
#     latest_version = last_version_finder.get_latest_version_for(repo)
#     next_version = latest_version.get_next()
#     next_versions.append(next_version)
#     print(repo + " " + str(next_version))
#
# # Шаг 3. Создаем все необходимые версии (релизы) в Jira
# for version in next_versions:
#     jira_client.create_version(version)
#
# print("Новые релизы в Jira созданы")
#
# # Шаг 4. Привязываем вновь созданные версии к нашей задаче
# jira_client.link_versions(
#     issue.key,
#     next_versions
# )
#
# print("Релизы привязаны к задаче")

bitbucketClient = BitbucketClient()

# # Шаг 5. Создаем ветки под наши версии в BitBucket
# for version in next_versions:
#     bitbucketClient.create_hotfix_branch(version)
#
# print("Ветки под релизы созданы\n")
#
# # Шаг 6. Находм пулл-реквесты из нашей задачной ветки в master. Если такого пулл-реквеста нет, то падаем с ошибкой.
# # TODO: создаем соответствующий пулл-реквест
#
# relevant_prs = []
# for repo in affected_repos:
#
#     prs = bitbucketClient.get_open_pull_requests(repo)
#     # Сохраняем текущее количество найденных PR, чтобы потом узнать, добавился ли еще один
#     len_relevant_prs_before = len(relevant_prs)
#     for pr in prs:
#         if pr['source']['branch']['name'] == issue.key and pr['destination']['branch']['name'] == 'master':
#             my_pr = PullRequest(
#                 repo,
#                 pr['source']['branch']['name'],
#                 pr['destination']['branch']['name'],
#                 pr['id'],
#                 pr['title']
#             )
#             relevant_prs.append(my_pr)
#     if len_relevant_prs_before == len(relevant_prs):  # Собственно проверяем, удалось ли найти хоть один новый PR
#         raise Exception('Could not find PR from issue branch to master branch: ')
#
# for pr in relevant_prs:
#     print("Текуший пулл-реквест: " + str(pr))
#
# # Шаг 7. Изменяем destination пулл-реквестов локально.
# for pr in relevant_prs:
#     pr.destination_branch = 'hotfix/' + next((x for x in next_versions if x.repo == pr.repo), None).version
#
# for pr in relevant_prs:
#     print("Пулл-реквест с измененным destination: " + str(pr))
#
# # Делаем update каждому пулл-реквесту
#
# for pr in relevant_prs:
#     bitbucketClient.update_pull_request(pr)
#
# print("\nПул реквесты обновлены")
#
# # Определяем, есть ли конфликты в нашем пулл-реквесте, и если есть, то ограничены ли они "допустимыми" строками
# #
# # for pr in relevant_prs:
# #     if conflictChecker.is_conflict_free(pr):
# #         merge(pr)
# #     elif conflictChecker.has_acceptable_conflicts(pr):
# #         slozhny_merge(pr)
# #     else:
# #         print('PR #' + pr.id + ' from ' + pr.source_branch + ' to ' + pr.destination_branch + ' in repo ' + pr.repo +
# #               ' has conflicts that must be resolved by hand.')
#
#
# # Делаем approve и merge для наших пул реквестов.
#
# # Шаг 8. Делаем approve и merge для пулл-реквестов.
#
# for pr in relevant_prs:
#     bitbucketClient.approve_pr(pr)
#     bitbucketClient.merge_pr(pr)
#
# print("Пулл-реквесты приняты")
#
# # Шаг 9. Готовим данные для новых пулл-реквестов
# new_relevant_prs = []
# for pr in relevant_prs:
#     new_pr = NewPullRequest(
#         pr.repo,
#         pr.destination_branch,
#         "master"
#     )
#     new_relevant_prs.append(new_pr)
#
# for pr in new_relevant_prs:
#     pr.filenames = pr.get_file_names()
#     pr.next_version = pr.get_next_version()
#     pr.current_version = pr.get_current_version()
#
# for pr in new_relevant_prs:
#     print("Новый пулл-реквест подготовлен: " + str(pr))
#
# # Если среди пулл-реквестов есть пулл-реквест с репозиторием commons,
# # то выносим отдельно текщую и следующую версии. Они нам понадобятся, для изменения данных в файле build.sbt.
# if new_relevant_prs[0].repo == "commons":
#     commons_version = new_relevant_prs[0].current_version
#     next_commons_version = new_relevant_prs[0].next_version
#     print(
#         "В задаче исправлен commons.\nТекущая версия commons: " + commons_version + "\nРелизная версия commons: " + next_commons_version)
# else:
#     commons_version = None
#     next_commons_version = None
#     print("В этой задаче commons не задет")
#
# # Создаем новые пулл-реквесты
# for pr in new_relevant_prs:
#     pr.id = bitbucketClient.create_pr(pr)
#
# print("Новые пулл-реквесты созданы")
#
# # Шаг 10. Получаем хэш ветки, выкачиваем соответствующие файлы, локально редактируем их и отправляем их обратно вместе с коммитом.
#
# for pr in new_relevant_prs:
#     pr.branch_hash = bitbucketClient.get_branch(pr)["target"]["hash"]
#
#     if type(pr.filenames) == list:
#         for filename in pr.filenames:
#             file_destination = pr.repo + "/" + filename
#             file = bitbucketClient.get_file_from_branch(pr, filename)
#             open(file_destination, 'wb').write(file.content)
#             print("Файл " + filename + " скачан из ветки " + pr.source_branch)
#             forward_to_change_file(file_destination, pr, issue_key, commons_version, next_commons_version)
#             print("Файл " + filename + " изменен")
#             bitbucketClient.upload_file(pr, filename, file_destination)
#             print("Файл " + filename + " загружен. Создан новый коммит")
#     else:
#         file_destination = pr.repo + "/" + pr.filenames
#         file = bitbucketClient.get_file_from_branch(pr, pr.filenames)
#         open(file_destination, 'wb').write(file.content)
#         print("Файл " + pr.filenames + " скачан из ветки " + pr.source_branch)
#         forward_to_change_file(file_destination, pr, issue_key, commons_version, next_commons_version)
#         print("Файл " + pr.filenames + " изменен")
#         bitbucketClient.upload_file(pr, pr.filenames, file_destination)
#         print("Файл " + pr.filenames + " загружен. Создан новый коммит")
#
# print("\nФайлы для всех репозиториев обновлены\n")
#
# # Шаги 11-12:
# # Делаем approve и merge для пулл-реквестов
# # Ставим тег на merge коммит.
#
# for pr in new_relevant_prs:
#     bitbucketClient.approve_pr(pr)
#     pr.merge_hash = bitbucketClient.merge_pr(pr)
#     print(pr.merge_hash)
#     bitbucketClient.add_tag_to_merge_commit(pr)


pr = PullRequest(
    'front',
    'coflict_test',
    'master',
    28,
    'Coflict test'
)

pr.filename = pr.get_file_names()
pr.diff_status = bitbucketClient.get_pr_conflict_status(pr)


conflictChecker = ConflictChecker()

if conflictChecker.is_conflict_free(pr):
    print('Конфликтов нет')
elif conflictChecker.has_acceptable_conflicts(pr):
    print('slozhny_merge')
else:
    print('Есть конфликты, но их нужно решать вручную')



