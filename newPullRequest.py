class NewPullRequest:

    def __init__(self, repo, source_branch, destination_branch):
        self.repo = repo
        self.source_branch = source_branch
        self.destination_branch = destination_branch

    def __str__(self):
        return "Репозиторий: " + self.repo \
               + "\nИз какой ветки: " + self.source_branch \
               + "\nВ какую ветку: " + self.destination_branch \
               + "\nТекущая версия: " + self.current_version \
               + "\nСледующая версия: " + self.next_version

    def get_file_names(self):
        if self.repo in ['id-ui', 'app-ui', 'operator-ui']:
            filename = 'src/environments/version.ts'
        elif self.repo in ['auth', 'front', 'file', 'ocr', 'operator-queue']:
            filename = ['build.sbt', 'conf/version']
        elif self.repo == 'commons':
            filename = 'pom.xml'
        return filename

    def get_next_version(self):
        version = self.source_branch.split("/", 1)[1]
        return version

    def get_current_version(self):
        versions = self.next_version.split(".", 2)
        hotfix_version = int(versions[2]) - 1
        version = versions[0] + "." + versions[1] + "." + str(hotfix_version)
        return version
