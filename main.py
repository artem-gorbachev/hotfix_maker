from jiraTask import JiraTask
from jiraClient import JiraClient
from lastVersionFinder import LastVersionFinder
from bitbucketClient import BitbucketClient
from pullRequest import PullRequest

issue_key = 'EN-1070'

issue = JiraTask(issue_key)

jira_client = JiraClient()

# Получаем Issue Id в обмен на Issue Key, т.к. для дальнейших операций нам понабится именно он
issue.id = jira_client.get_issue_id(issue.key)
print(issue.id)

# Получаем список репозиториев, которые были затронуты в задаче
affected_repos = jira_client.get_issue_repos(issue.id)
print(affected_repos)

last_version_finder = LastVersionFinder()

# Получаем список версий (релизов), которые будут созданы под создаваемый хотфикс
next_versions = []
for repo in affected_repos:
    latest_version = last_version_finder.get_latest_version_for(repo)
    next_version = latest_version.get_next()
    next_versions.append(next_version)

for x in next_versions:
    print(x)

# Создаем все необходимые версии (релизы) в Jira
for version in next_versions:
    jira_client.create_version(version)

# Привязываем вновь созданные версии к нашей задаче
jira_client.link_versions(
    issue.key,
    next_versions
)

bitbucketClient = BitbucketClient()

# Создаем ветки под наши версии в BitBucket
for version in next_versions:
    bitbucketClient.create_hotfix_branch(version)

# Находм пулл-реквесты из нашей задачной ветки в master. Если такого пулл-реквеста нет, то падаем с ошибкой.
# TODO: создаем соответствующий пулл-реквест

relevant_prs = []
for repo in affected_repos:

    prs = bitbucketClient.get_open_pull_requests(repo)
    # Сохраняем текущее количество найденных PR, чтобы потом узнать, добавился ли еще один
    len_relevant_prs_before = len(relevant_prs)
    for pr in prs:
        if pr['source']['branch']['name'] == issue.key and pr['destination']['branch']['name'] == 'master':
            my_pr = PullRequest(
                repo,
                pr['source']['branch']['name'],
                pr['destination']['branch']['name'],
                pr['id'],
                pr['title']
            )
            relevant_prs.append(my_pr)
    if len_relevant_prs_before == len(relevant_prs):  # Собственно проверяем, удалось ли найти хоть один новый PR
        raise Exception('Could not find PR from issue branch to master branch: ')

for pr in relevant_prs:
    print(pr)

# Изменяем destination пулл-реквестов локально.
for pr in relevant_prs:
    pr.destination_branch = 'hotfix/' + next((x for x in next_versions if x.repo == pr.repo), None).version
    #print(next((x for x in next_versions if x.repo == pr.repo), None).version)


for pr in relevant_prs:
    print(pr)

# Делаем update каждому пулл-реквесту

for pr in relevant_prs:
    bitbucketClient.update_pull_request(pr)

# Определяем, есть ли конфликты в нашем пулл-реквесте, и если есть, то ограничены ли они "допустимыми" строками

# for pr in relevant_prs:
#     if conflictChecker.is_conflict_free(pr):
#         merge(pr)
#     elif conflictChecker.has_acceptable_conflicts(pr):
#         slozhny_merge(pr)
#     else:
#         print('PR #' + pr.id + ' from ' + pr.source_branch + ' to ' + pr.destination_branch + ' in repo ' + pr.repo +
#               ' has conflicts that must be resolved by hand.')